#!/bin/bash

# Script pour lancer toutes vos stacks Docker Compose

# Assurez-vous d'être dans le répertoire contenant vos fichiers docker-compose.yml
cd r5.a.09-controle

# Lancer Traefik
docker compose Traefik/docker-compose.yml up 

# Lancer Joomla
docker compose Joomla/docker-compose.yml up 

# Lancer Minio
docker compose Minio/docker-compose.yml up 

# Lancer Magento
docker compose Magento/docker-compose.yml up 

# Lancer Wordpress
docker compose Wordpress/docker-compose.yml up 

# Lancer Loki
docker compose Loki/docker-compose.yml up 

