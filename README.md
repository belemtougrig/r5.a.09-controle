# Guide de Déploiement

Ce guide fournit des instructions pour déployer les différentes stacks Docker Compose.

## Prérequis

- Docker et Docker Compose installés sur votre machine.

## Instructions

1. Clonez le dépôt Git.

   ```bash
   git clone https://github.com/votre-utilisateur/votre-depot.git

   Excecuter init.sh